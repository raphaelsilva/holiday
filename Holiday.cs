using System;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Collections.Generic;
using System.Text.Json.Serialization;
using CsvHelper;
using System.Globalization;

namespace CodeHB.Holiday
{
	public static class Holiday
	{
		private const string GOV_API_BASE_URL = "https://www.gov.uk/bank-holidays.json";
		private const int DEFAULT_CALENDAR_YEAR = 2021;

		[FunctionName("Holiday")]
		public static async Task<IActionResult> Run(
			[HttpTrigger(AuthorizationLevel.Anonymous, "get", Route = null)] HttpRequest req,
			ILogger log, ExecutionContext context)
		{
			string data = req.Query["year"];

			int year = ParseInt(data);

			string responseMessage = string.IsNullOrEmpty(data)
				? "You have not provide a valid year or haven't provided an year at all. Please, provide a valid year value: yyyy."
				: $"Happy holidays!!.";

			DateTime baseDate = new DateTime(year, 01, 01);
			DateTime limitDate = baseDate.AddYears(1);

			try
			{
				var response = await GetDataAsync(year);

				var bankHolidays = DeserializeBankHolidays(response, log);
				var englandAndWhalesHolidays = GetEnglandAndWhalesHolidays(baseDate, limitDate, bankHolidays);
				ExportHolidaysToCsv(log, englandAndWhalesHolidays);

			}
			catch (HttpRequestException e)
			{
				log.LogError("\nException Caught!");
				log.LogError("Message :{0} ", e.Message);
			}

			return SendHolidaysAsCsv(log, year);
		}

		private static int ParseInt(string year, int defaultYear = DEFAULT_CALENDAR_YEAR) // arbitrary default year
		{
			int parsedYear;

			if (int.TryParse(year, out parsedYear))
			{
				return parsedYear;
			}
			return defaultYear;
		}

		private static async Task<string> GetDataAsync(int year)
		{
			using (var httpClient = new HttpClient())
			{
				using (var request = new HttpRequestMessage(new HttpMethod("GET"), GOV_API_BASE_URL))
				{
					var response = await httpClient.SendAsync(request);
					return await response.Content.ReadAsStringAsync();
				}
			}
		}


		private static IEnumerable<Event> GetEnglandAndWhalesHolidays(DateTime baseDate, DateTime limitDate, BankHolidays bankHolidays)
		{
			var englandAndWhales =
				from holidays in bankHolidays.EnglandAndWales.events
				where holidays.date >= baseDate && holidays.date < limitDate
				orderby holidays.date descending
				select holidays;

			return englandAndWhales;
		}

		private static dynamic DeserializeBankHolidays(string response, ILogger log)
		{
			try
			{
				return JsonSerializer.Deserialize<BankHolidays>(response);
			}
			catch (Exception e)
			{
				log.LogError($"An unpredicted error has ocurred on our end: {e.Message}");

				return string.Empty;
			}

		}

		private static void ExportHolidaysToCsv(ILogger log, IEnumerable<Event> holidays)
		{
			var path = Path.Combine(Path.GetTempPath(), "england-and-wales-holidays.csv");
			log.LogInformation($"Writing csv file at {path}");

			if (File.Exists(path))
			{
				log.LogInformation("Deleting existent resources...");
				File.Delete(path);
			}

			using (var writer = new StreamWriter(path))
			{
				using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
				{
					csv.WriteRecords(holidays);
				}
			}
		}

		private static FileContentResult SendHolidaysAsCsv(ILogger log, int year)
		{
			var path = Path.Combine(Path.GetTempPath(), "england-and-wales-holidays.csv");

			log.LogInformation($"Reading csv file from {path}");

			var csv = System.IO.File.ReadAllBytes(path);
			var fileResult = new FileContentResult(csv, "text/csv");
			fileResult.FileDownloadName = $"england-and-wales-holidays-{year}.csv";

			return fileResult;
		}

	}

	// Models
	public class Event
	{

		public string title { get; set; }
		public DateTime date { get; set; }
		public string notes { get; set; }
		public bool bunting { get; set; }
	}

	public class EnglandAndWales
	{
		public string division { get; set; }
		public List<Event> events { get; set; }
	}

	public class Scotland
	{
		public string division { get; set; }
		public List<Event> events { get; set; }
	}

	public class NorthernIreland
	{
		public string division { get; set; }
		public List<Event> events { get; set; }
	}

	public class BankHolidays
	{
		[JsonPropertyName("england-and-wales")]
		public EnglandAndWales EnglandAndWales { get; set; }
		public Scotland scotland { get; set; }

		[JsonPropertyName("northern-ireland")]
		public NorthernIreland NorthernIreland { get; set; }
	}

}
