# Holiday

`Holidays` act as a middleware between the UK government API and you, providing structured and sanitized data. In this case, literally holidays of a given year exported as a =csv= file.
## Calling it

URL: `https://ukholidays.azurewebsites.net/api/Holiday?year=2019`

The default value of the year is arbitrary `2021`.

# Deploy

The function is deployed in the UK South, therefore, is expected better performance for nearby users.

# Code
The code is somewhat straightforward, the =Run= function is the program entry point. Data serialization, sanitization, and API connection were encapsulated into small-single purpose helper functions.
## Log & Error handling
The code makes extensive usage of logging and reasonable error handling, e.g. =ParseInt= method. Use log made the debugging process while developing easy.

## Data serialization and deserialization

I opted for declared response models for deserializing the data instead of manipulating the "stringified" API response.
Perhaps, fewer lines of code would be used, but on the other hand, I'd have less control over the data, especially data types.

It ended up being also useful for exporting the data as =cav=.
## Memory

Each holiday report is persisted into a temporary file that is deleted and recreated on every call. With that we benefit of "personalized" filename output, eg. =$"england-and-wales-holidays-{year}.csv"=, and memory usage effectiveness.
